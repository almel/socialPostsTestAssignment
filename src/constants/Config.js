export const DEFAULT_API_URL = 'http://api.massrelevance.com/MassRelDemo/kindle.json';

export const DEFAULT_NUMBER_OF_POSTS = 10;

export const DEFAULT_UPDATE_INTERVAL = 300;
